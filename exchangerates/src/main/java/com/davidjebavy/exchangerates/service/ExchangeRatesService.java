package com.davidjebavy.exchangerates.service;

import com.davidjebavy.exchangerates.entity.ExchangeRate;
import com.davidjebavy.exchangerates.repositories.ExchangeRatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExchangeRatesService {

    private CSASConnector csasConnector;

    private ExchangeRatesRepository repository;

    @Autowired
    public ExchangeRatesService(CSASConnector csasConnector, ExchangeRatesRepository repository) {
        this.csasConnector = csasConnector;
        this.repository = repository;
    }

    /**
     * Retrieve rates from DB or from API if update is true.
     *
     * @param update: flag if rates should be updated before returning them.
     * @return
     */
    public List<ExchangeRate> getRates(boolean update) {
        return update ? updateRates() : repository.findAll();
    }

    /**
     * Get updated rates and save them to DB
     *
     * @return List of updated exchange rates
     */
    public List<ExchangeRate> updateRates() {
        List<ExchangeRate> newRates = csasConnector.getRates();
        repository.saveAll(newRates);

        return newRates;
    }


}
