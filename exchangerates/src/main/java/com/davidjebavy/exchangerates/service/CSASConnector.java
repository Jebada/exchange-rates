package com.davidjebavy.exchangerates.service;

import com.davidjebavy.exchangerates.entity.ExchangeRate;
import com.davidjebavy.exchangerates.exception.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CSASConnector {

    private UrlBuilder urlBuilder;

    private final String URL;
    private final String WEBAPIKEY;

    private final RestTemplate restTemplate;

    @Autowired
    public CSASConnector(UrlBuilder urlBuilder,
                         @Value("${data.url}") String url,
                         @Value("${data.web-api-key}") String webApiKey,
                         RestTemplateBuilder restTemplateBuilder) {
        this.urlBuilder = urlBuilder;
        URL = url;
        WEBAPIKEY = webApiKey;
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Connect to CSAS api and get updated rates.
     * Check response
     * @return List of updated exchange rates
     */
    public List<ExchangeRate> getRates() {
        String url = UrlBuilder.builder()
                .webApiKey(WEBAPIKEY)
                .URL(URL)
                .build()
                .getUrl();

        try {
            // Get data from resource
            var response = restTemplate
                    .getForEntity(url, ExchangeRate[].class);

            if(response.getBody() == null) {
                throw new ClientException("Data was empty", response.getStatusCode());
            }

            return Arrays.stream(response.getBody()).collect(Collectors.toList());

        } catch (HttpClientErrorException e) {
            String message = "Getting data from " + url + " failed.";
            log.error(message);
            throw new ClientException("Connection to provider failed", e.getStatusCode());
        }
    }

}
