package com.davidjebavy.exchangerates.repositories;

import com.davidjebavy.exchangerates.entity.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRatesRepository extends JpaRepository<ExchangeRate, Long> {

}
