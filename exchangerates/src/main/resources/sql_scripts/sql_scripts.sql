DROP USER IF EXISTS 'demo'@'localhost';

CREATE USER 'demo'@'localhost' IDENTIFIED BY 'demo';

GRANT ALL PRIVILEGES ON *.* TO 'demo'@'localhost';

DROP DATABASE IF EXISTS `rates`;

CREATE DATABASE `rates`;

USE `rates`;

DROP TABLE IF EXISTS `exchange_rates`;

CREATE TABLE `exchange_rates` (
	`name` VARCHAR(15) NOT NULL,
    `amount` INT DEFAULT NULL,
    `cnb_mid` FLOAT DEFAULT NULL,
    `country` VARCHAR(45) DEFAULT NULL,
    `curr_buy` FLOAT DEFAULT NULL,
    `curr_mid` FLOAT DEFAULT NULL,
    `curr_sell` FLOAT DEFAULT NULL,
    `ecb_mid` FLOAT DEFAULT NULL,
    `move` FLOAT DEFAULT NULL,
    `short_name` VARCHAR(5) DEFAULT NULL,
    `val_buy` FLOAT DEFAULT NULL,
    `val_mid` FLOAT DEFAULT NULL,
    `val_sell` FLOAT DEFAULT NULL,
    `valid_from` DATETIME DEFAULT NULL,
	`version` INT DEFAULT NULL,


    PRIMARY KEY(`name`)

)ENGINE=InnoDb CHARSET=utf8;
